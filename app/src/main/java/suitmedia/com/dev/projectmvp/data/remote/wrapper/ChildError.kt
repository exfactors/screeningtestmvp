package suitmedia.com.dev.projectmvp.data.remote.wrapper


/**
 * Created by Andri Dwi Utomo on 9/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
class ChildError {
    var data: Map<String, List<String>>? = null
}