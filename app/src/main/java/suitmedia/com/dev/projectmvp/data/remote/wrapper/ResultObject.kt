package suitmedia.com.dev.projectmvp.data.remote.wrapper

import com.google.gson.annotations.SerializedName


/**
 * Created by Andri Dwi Utomo on 9/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
class ResultObject<T> {

    @SerializedName("status")
    var status: Int? = 0

    @SerializedName("message")
    var message: String? = ""

    @SerializedName("results")
    var result: T? = null

}