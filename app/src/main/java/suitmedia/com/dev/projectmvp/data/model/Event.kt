package suitmedia.com.dev.projectmvp.data.model

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey


/**
 * Created by Andri Dwi Utomo on 7/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
class Event : RealmObject {
    var id: Int = 0

    var name: String = ""
}