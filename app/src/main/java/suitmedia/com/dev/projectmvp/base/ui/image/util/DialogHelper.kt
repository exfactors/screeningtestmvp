package suitmedia.com.dev.projectmvp.base.ui.image.util


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AlertDialog
import suitmedia.com.dev.projectmvp.R
import suitmedia.com.dev.projectmvp.base.ui.image.constant.ImageProvider
import suitmedia.com.dev.projectmvp.base.ui.image.listener.DismissListener
import suitmedia.com.dev.projectmvp.base.ui.image.listener.ResultListener

/**
 * Created by Andri Dwi Utomo on 14/11/2021.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
internal object DialogHelper {

    /**
     * Show Image Provide Picker Dialog. This will streamline the code to pick/capture image
     *
     */
    fun showChooseAppDialog(
        context: Context,
        listener: ResultListener<ImageProvider>,
        dismissListener: DismissListener?
    ) {
        val layoutInflater = LayoutInflater.from(context)
        val customView = layoutInflater.inflate(R.layout.dialog_picker_image, null)

        val dialog = AlertDialog.Builder(context)
            .setTitle(R.string.txt_dialog_title_pick_image)
            .setView(customView)
            .setOnCancelListener {
                listener.onResult(null)
            }
            .setNegativeButton(R.string.txt_title_cancel) { _, _ ->
                listener.onResult(null)
            }
            .setOnDismissListener {
                dismissListener?.onDismiss()
            }
            .show()

        // Handle Camera option click
        customView.findViewById<View>(R.id.linCameraPick).setOnClickListener {
            listener.onResult(ImageProvider.CAMERA)
            dialog.dismiss()
        }

        // Handle Gallery option click
        customView.findViewById<View>(R.id.linGalleryPick).setOnClickListener {
            listener.onResult(ImageProvider.GALLERY)
            dialog.dismiss()
        }
    }
}