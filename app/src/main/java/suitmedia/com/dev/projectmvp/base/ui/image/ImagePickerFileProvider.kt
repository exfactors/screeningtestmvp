package suitmedia.com.dev.projectmvp.base.ui.image

import androidx.core.content.FileProvider


/**
 * Created by Andri Dwi Utomo on 14/11/2021.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
class ImagePickerFileProvider : FileProvider()