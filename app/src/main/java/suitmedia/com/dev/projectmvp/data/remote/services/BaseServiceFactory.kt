package suitmedia.com.dev.projectmvp.data.remote.services

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import suitmedia.com.dev.projectmvp.BuildConfig
import suitmedia.com.dev.projectmvp.data.local.prefs.DataConstant
import suitmedia.com.dev.projectmvp.data.local.prefs.SuitPreferences
import java.util.concurrent.TimeUnit


/**
 * Created by Andri Dwi Utomo on 9/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
object BaseServiceFactory {

    fun getAPIService(): APIService {
        return provideRetrofit(makeGSON())
    }

    private fun provideRetrofit(gSon: Gson): APIService {

        var url: String
        val currentUrl: String? = SuitPreferences.instance()?.getString(DataConstant.BASE_URL)

        if (currentUrl != null && currentUrl.isNotEmpty()) {
            currentUrl.let { url = currentUrl }
        } else {
            url = BuildConfig.API_URL
        }

        val retrofit = Retrofit.Builder()
            .baseUrl(url)
            .client(provideOkHttpClient())
            .addConverterFactory(GsonConverterFactory.create(gSon))
            .build()

        return retrofit.create(APIService::class.java)
    }

    private fun provideOkHttpClient(): OkHttpClient {

        val httpClient = OkHttpClient().newBuilder()
            .apply {
                readTimeout(60, TimeUnit.SECONDS)
                connectTimeout(60, TimeUnit.SECONDS)
                writeTimeout(60, TimeUnit.SECONDS)
            }

        if (BuildConfig.DEBUG) {
            httpClient.addInterceptor(LoggingInterceptor.getLoggingInterceptor())
        }

        httpClient.addNetworkInterceptor(NetworkInterceptor())
        httpClient.addInterceptor(ErrorInterceptor())
        //httpClient.addInterceptor(NetworkConnectionInterceptor(BaseApplication.appContext))

        return httpClient.build()
    }

    private fun makeGSON(): Gson {
        return GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            //.registerTypeAdapter(Home::class.java, HomeDeserializer())
            //.registerTypeAdapter(Errors::class.java, ErrorsDeserializer())
            .create()
    }

}