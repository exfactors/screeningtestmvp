package suitmedia.com.dev.projectmvp.feature.screenfour

import android.widget.TextView
import suitmedia.com.dev.projectmvp.base.ui.adapter.viewholder.BaseItemViewHolder
import suitmedia.com.dev.projectmvp.data.model.ListEvent
import suitmedia.com.dev.projectmvp.data.model.Users
import suitmedia.com.dev.projectmvp.databinding.ItemListGuestBinding
import suitmedia.com.dev.projectmvp.feature.screenthreefive.three.ListEventItemView


/**
 * Created by Andri Dwi Utomo on 10/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
class ListGuestItemView(var binding: ItemListGuestBinding) : BaseItemViewHolder<Users>(binding) {

    private var mActionListener: ListGuestItemView.OnActionListener? = null
    private var listGuest: Users? = null

    override fun bind(data: Users?) {
        data?.let {
            listGuest = data
            binding.model = data

            binding.layGetGuest.setOnClickListener {
                if (mActionListener != null) {
                    mActionListener?.onClicked(this, adapterPosition - 1)
                }
            }
        }
    }

    fun getTitleView() : TextView {
        return binding.tvTitle
    }

    fun getData() : Users? {
        return listGuest
    }

    fun setOnActionListener(listener: OnActionListener) {
        mActionListener = listener
    }

    interface OnActionListener {
        fun onClicked(view: ListGuestItemView, position: Int)
    }

}