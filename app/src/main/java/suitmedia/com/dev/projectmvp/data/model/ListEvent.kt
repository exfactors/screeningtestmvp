package suitmedia.com.dev.projectmvp.data.model

import com.google.gson.annotations.SerializedName


/**
 * Created by Andri Dwi Utomo on 10/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
open class ListEvent {
    @SerializedName("image")
    val image: String? = ""

    @SerializedName("title")
    val title: String? = ""

    @SerializedName("description")
    val description: String? = ""

    @SerializedName("date")
    val date: String? = ""

    @SerializedName("time")
    val time: String? = ""

    @SerializedName("latitude")
    val latitude: Double? = 0.0

    @SerializedName("longitude")
    val longitude: Double? = 0.0

}