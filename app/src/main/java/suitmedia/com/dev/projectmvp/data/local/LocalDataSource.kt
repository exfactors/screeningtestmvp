package suitmedia.com.dev.projectmvp.data.local

import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.RealmResults
import io.realm.query
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import suitmedia.com.dev.projectmvp.data.model.*


/**
 * Created by Andri Dwi Utomo on 7/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
class LocalDataSource {
    val config = RealmConfiguration.Builder(schema = setOf(Profiles::class, Event::class, Guest::class, GuestApi::class))
        .schemaVersion(1)
        .deleteRealmIfMigrationNeeded()
        .build()
    val realm: Realm = Realm.open(config)

    suspend fun addProfile(names: String, images: String) = withContext(Dispatchers.IO) {
        realm.write {
            // fetch a frog from the realm based on some query
            val profile: Profiles? =
                this.query<Profiles>("id == 1").first().find()
            // if the query returned an object, update object from the query
            if (profile != null) {
                profile.name = names
                profile.image = images
            } else {
                // if the query returned no object, insert a new object with a new primary key.
                this.copyToRealm(Profiles().apply {
                    id = 1
                    name = names
                    image = images
                })
            }
        }

    }

    fun getProfile() : Profiles? {
        val profiles: Profiles? = realm.query<Profiles>("id == 1").first().find()

        return profiles
    }

    suspend fun addEvent(names: String) = withContext(Dispatchers.IO){
        realm.write {
            // fetch a frog from the realm based on some query
            val event: Event? =
                this.query<Event>("id == 1").first().find()
            // if the query returned an object, update object from the query
            if (event != null) {
                event.name = names
            } else {
                // if the query returned no object, insert a new object with a new primary key.
                this.copyToRealm(Event().apply {
                    id = 1
                    name = names
                })
            }
        }
    }

    fun getEvent() : Event? {
        val event: Event? = realm.query<Event>("id == 1").first().find()

        return event
    }

    suspend fun addGuest(guest: String) = withContext(Dispatchers.IO){
        realm.write {
            // fetch a frog from the realm based on some query
            val gModel: Guest? =
                this.query<Guest>("id == 1").first().find()
            // if the query returned an object, update object from the query
            if (gModel != null) {
                gModel.name = guest
            } else {
                // if the query returned no object, insert a new object with a new primary key.
                this.copyToRealm(Guest().apply {
                    id = 1
                    name = guest
                })
            }
        }
    }

    fun getGuest() : Guest? {
        val guest: Guest? = realm.query<Guest>("id == 1").first().find()

        return guest
    }

    fun addGuestApi(gApi: GuestApi) {
        realm.writeBlocking {
            // fetch a frog from the realm based on some query
            val gModel: GuestApi? =
                this.query<GuestApi>("id == $0", gApi.id).first().find()
            // if the query returned an object, update object from the query
            if (gModel != null) {
                gModel.id = gApi.id
                gModel.email = gApi.email
                gModel.avatar = gApi.avatar
                gModel.firstName = gApi.firstName
                gModel.lastName = gApi.lastName
            } else {
                // if the query returned no object, insert a new object with a new primary key.
                this.copyToRealm(gApi)
            }
        }

    }

    fun getAllGuestApi() : RealmResults<GuestApi>? {
        val guest: RealmResults<GuestApi> = realm.query<GuestApi>().find()

        return guest
    }
}