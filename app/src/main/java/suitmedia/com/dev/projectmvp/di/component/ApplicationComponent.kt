package suitmedia.com.dev.projectmvp.di.component

import dagger.Component
import suitmedia.com.dev.projectmvp.di.module.ApplicationModule
import suitmedia.com.dev.projectmvp.di.scope.SuitCoreApplicationScope
import suitmedia.com.dev.projectmvp.feature.screenfour.ScreenFourPresenter
import suitmedia.com.dev.projectmvp.feature.screenone.ScreenOnePresenter
import suitmedia.com.dev.projectmvp.feature.screenthreefive.base.ScreenBasePresenter
import suitmedia.com.dev.projectmvp.feature.screenthreefive.five.ScreenFivePresenter
import suitmedia.com.dev.projectmvp.feature.screenthreefive.three.ScreenThreePresenter
import suitmedia.com.dev.projectmvp.feature.screentwo.ScreenTwoPresenter
import suitmedia.com.dev.projectmvp.firebase.remoteconfig.RemoteConfigPresenter
import suitmedia.com.dev.projectmvp.onesignal.OneSignalPresenter


/**
 * Created by Andri Dwi Utomo on 9/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
@SuitCoreApplicationScope
@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {
    fun inject(remoteConfigPresenter: RemoteConfigPresenter)
    fun inject(screenOnePresenter: ScreenOnePresenter)
    fun inject(screenTwoPresenter: ScreenTwoPresenter)
    fun inject(screenBasePresenter: ScreenBasePresenter)
    fun inject(screenThreePresenter: ScreenThreePresenter)
    fun inject(screenFivePresenter: ScreenFivePresenter)
    fun inject(screenFourPresenter: ScreenFourPresenter)
    fun inject(oneSignalPresenter: OneSignalPresenter)

}