package suitmedia.com.dev.projectmvp.data.model

import io.realm.RealmObject


/**
 * Created by Andri Dwi Utomo on 7/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
class Task : RealmObject {
    var name: String = "new task"
    var status: String = "Open"
}