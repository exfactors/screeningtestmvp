package suitmedia.com.dev.projectmvp.feature.screenthreefive.base

import android.graphics.drawable.Drawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import suitmedia.com.dev.projectmvp.R
import suitmedia.com.dev.projectmvp.base.ui.BaseActivity
import suitmedia.com.dev.projectmvp.databinding.ActivityBaseThreeFiveBinding
import suitmedia.com.dev.projectmvp.feature.screenone.ScreenOnePresenter
import suitmedia.com.dev.projectmvp.feature.screenthreefive.three.ScreenThreeFragment
import suitmedia.com.dev.projectmvp.feature.screentwo.ScreenTwoActivity


/**
 * Created by Andri Dwi Utomo on 10/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
class ScreenBaseActivity : BaseActivity<ActivityBaseThreeFiveBinding>(), ScreenBaseView {
    private var screenBasePresenter: ScreenBasePresenter? = null
    private var state: String = "maps"

    override fun getViewBinding() = ActivityBaseThreeFiveBinding.inflate(layoutInflater)

    override fun onViewReady(savedInstanceState: Bundle?) {
        setupPresenter()
        setupFragment()
        binding.toolbar.ivToolbarIconMap.setOnClickListener { screenBasePresenter!!.changeLayout(state) }
        binding.toolbar.ivToolbarIconLeft.setOnClickListener {
            goToActivity(ScreenTwoActivity::class.java,  null, clearIntent = true, isFinish = true)
        }
    }

    private fun setupPresenter() {
        screenBasePresenter = ScreenBasePresenter(this, resources)
        /*screenBasePresenter!!.changeLayout(state)*/
    }

    private fun setupFragment() {
        replace(state, ScreenThreeFragment())

    }

    fun replace(s: String, f: Fragment) {
        val ft: FragmentTransaction = supportFragmentManager.beginTransaction()!!
        ft.replace(R.id.frameLayout, f, s)
        ft.commit()
        ft.addToBackStack(null)
    }

    private fun setContentFragment(fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.frameLayout, fragment)
            .commitAllowingStateLoss()
    }

    override fun onChangeLayout(state: String, fragment: Fragment, drawable: Drawable) {
        this.state = state
        binding.toolbar.ivToolbarIconMap.setImageDrawable(drawable)
        replace(state, fragment)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        goToActivity(ScreenTwoActivity::class.java,  null, clearIntent = true, isFinish = true)

    }

}