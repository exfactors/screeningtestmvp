package suitmedia.com.dev.projectmvp.onesignal

import io.reactivex.disposables.CompositeDisposable
import suitmedia.com.dev.projectmvp.BaseApplication
import suitmedia.com.dev.projectmvp.base.presenter.BasePresenter
import suitmedia.com.dev.projectmvp.data.remote.services.APIService
import javax.inject.Inject


/**
 * Created by Andri Dwi Utomo on 10/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
class OneSignalPresenter(var view: OneSignalView) : BasePresenter() {
    @Inject
    lateinit var apiService: APIService
    private var mvpView: OneSignalView? = null
    private var mCompositeDisposable: CompositeDisposable? = CompositeDisposable()

    init {
        BaseApplication.applicationComponent.inject(this)
        declareView()
    }

    fun registerPlayerId(playerId: String){
        // Register Player id to server

    }

    private fun declareView(){
        mvpView = view
        addToLifeCycle(mvpView!!, null)
    }
}