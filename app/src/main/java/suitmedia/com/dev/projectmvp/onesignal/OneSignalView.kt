package suitmedia.com.dev.projectmvp.onesignal

import suitmedia.com.dev.projectmvp.base.presenter.MvpView


/**
 * Created by Andri Dwi Utomo on 10/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
interface OneSignalView : MvpView {

    fun onRegisterIdSuccess(message: String?)

    fun onRegisterIdFailed(error: Any?)

}