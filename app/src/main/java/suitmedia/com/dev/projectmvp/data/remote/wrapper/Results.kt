package suitmedia.com.dev.projectmvp.data.remote.wrapper

import com.google.gson.annotations.SerializedName


/**
 * Created by Andri Dwi Utomo on 9/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
class Results<T> {

    @SerializedName("page")
    var page: Int? = 0

    @SerializedName("per_page")
    var perPage: Int? = 0

    @SerializedName("total")
    var total: Int? = 0

    @SerializedName("total_pages")
    var totalPages: Int? = 0

    @SerializedName("data")
    var arrayData: List<T>? = null
}