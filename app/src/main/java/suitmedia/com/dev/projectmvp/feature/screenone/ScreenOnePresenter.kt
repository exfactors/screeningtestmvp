package suitmedia.com.dev.projectmvp.feature.screenone

import android.net.Uri
import kotlinx.coroutines.*
import suitmedia.com.dev.projectmvp.BaseApplication
import suitmedia.com.dev.projectmvp.base.presenter.BasePresenter
import suitmedia.com.dev.projectmvp.data.local.LocalDataSource
import suitmedia.com.dev.projectmvp.helper.CustomSetting
import kotlin.coroutines.CoroutineContext


/**
 * Created by Andri Dwi Utomo on 9/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
class ScreenOnePresenter(var view: ScreenOneView) : BasePresenter(), CoroutineScope {
    private val job = Job()
    override val coroutineContext: CoroutineContext = job + Dispatchers.IO

    private var localDataSource: LocalDataSource = LocalDataSource()

    private var mvpView: ScreenOneView? = null

    init {
        BaseApplication.applicationComponent.inject(this)
        declareView()
    }

    fun checkPalindrome(text: String) {
        var message: String = ""
        if (CustomSetting.isPalindromeString(text)) {
            message = "Text is palindrome"
        } else {
            message = "Text is not palindrome"
        }

        view.onCheckPalindrome(message)
    }

    fun saveProfile(name: String, image: String) = launch(Dispatchers.Main) {

        if (name.equals("")) {
            view.onFailedSave("Name is required")
        } else if (Uri.parse(image) == null) {
            view.onFailedSave("Image is required")
        } else {
            localDataSource.addProfile(name, image)
            view.onSuccessSave("Success to save")
        }
    }

    fun getProfile() {
        val profile = localDataSource.getProfile()
        if (profile != null) {
            view.onGetProfile(profile)
        }
    }

    private fun declareView() {
        mvpView = view
        addToLifeCycle(mvpView!!, job)
    }
}