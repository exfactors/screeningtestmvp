package suitmedia.com.dev.projectmvp.feature.screentwo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import suitmedia.com.dev.projectmvp.base.ui.BaseActivity
 import suitmedia.com.dev.projectmvp.databinding.ActivityScreenTwoBinding
import suitmedia.com.dev.projectmvp.feature.screenfour.ScreenFourActivity
import suitmedia.com.dev.projectmvp.feature.screenthreefive.base.ScreenBaseActivity

/**
 * Created by Andri Dwi Utomo on 10/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
class ScreenTwoActivity : BaseActivity<ActivityScreenTwoBinding>(), ScreenTwoView {
    private var screenTwoPresenter: ScreenTwoPresenter? = null
    override fun getViewBinding() = ActivityScreenTwoBinding.inflate(layoutInflater)

    override fun onViewReady(savedInstanceState: Bundle?) {
        setupPresenter()
        actionClick()
    }

    private fun setupPresenter() {
        screenTwoPresenter = ScreenTwoPresenter(this)
        screenTwoPresenter!!.getName()
        screenTwoPresenter!!.getEvent()
        screenTwoPresenter!!.getGuest()
    }

    private fun actionClick() {
        binding.btnEvent.setOnClickListener {
            goToActivity(ScreenBaseActivity::class.java,  null, clearIntent = true, isFinish = true)
        }

        binding.btnGuest.setOnClickListener {
            goToActivity(ScreenFourActivity::class.java,  null, clearIntent = true, isFinish = true)

        }
    }

    override fun onGetName(name: String) {
        binding.tvName.setText(name)
    }

    override fun onGetEvent(name: String) {
        binding.btnEvent.setText(name)
    }

    override fun onGetGuest(name: String) {
        binding.btnGuest.setText(name)
    }

}