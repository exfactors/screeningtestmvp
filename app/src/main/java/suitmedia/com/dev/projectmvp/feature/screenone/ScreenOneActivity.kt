package suitmedia.com.dev.projectmvp.feature.screenone

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import android.widget.Toast
import suitmedia.com.dev.projectmvp.R
import suitmedia.com.dev.projectmvp.base.ui.BaseActivity
import suitmedia.com.dev.projectmvp.base.ui.image.ImagePicker
import suitmedia.com.dev.projectmvp.data.model.Profiles
import suitmedia.com.dev.projectmvp.data.model.UpdateType
import suitmedia.com.dev.projectmvp.databinding.ActivityScreenOneBinding
import suitmedia.com.dev.projectmvp.feature.screentwo.ScreenTwoActivity
import suitmedia.com.dev.projectmvp.firebase.remoteconfig.RemoteConfigPresenter
import suitmedia.com.dev.projectmvp.firebase.remoteconfig.RemoteConfigView

/**
 * Created by Andri Dwi Utomo on 9/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
class ScreenOneActivity : BaseActivity<ActivityScreenOneBinding>(), ScreenOneView, RemoteConfigView {

    private var screenOnePresenter: ScreenOnePresenter? = null
    private var remoteConfigPresenter: RemoteConfigPresenter? = null

    override fun getViewBinding() = ActivityScreenOneBinding.inflate(layoutInflater)

    private var image: Uri? = null

    override fun onViewReady(savedInstanceState: Bundle?) {
        setupPresenter()
        setupActionClick()


    }

    private fun setupPresenter() {
        screenOnePresenter = ScreenOnePresenter(this)
        remoteConfigPresenter = RemoteConfigPresenter(this)
        screenOnePresenter!!.getProfile()
        remoteConfigPresenter!!.getVersion()
    }

    private fun setupActionClick() {
        binding.relProfile.setOnClickListener {
            dialogPickImage()
        }
        binding.btnCheck.setOnClickListener {
            screenOnePresenter!!.checkPalindrome(binding.etPalindrome.text.toString())
        }
        binding.btnNext.setOnClickListener {
            screenOnePresenter!!.saveProfile(binding.etName.text.toString(), image.toString())
        }
    }

    private fun dialogPickImage() {
        val dialogBuilder = AlertDialog.Builder(this)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialogBuilder.setView(R.layout.layout_dialog_image_picker)
        } else {
            val dialogView = LayoutInflater.from(this)
                .inflate(R.layout.layout_dialog_image_picker, null, false)
            dialogBuilder.setView(dialogView)
        }
        dialogBuilder.create()
            .also {
                it.show()
                it.findViewById<TextView>(R.id.tvDialogTitle).text = getString(R.string.txt_dialog_title_pick_image)
                it.findViewById<View>(R.id.tvActionGallery).setOnClickListener { _ ->
                    it.dismiss()
                    openGallery()
                }
                it.findViewById<View>(R.id.tvActionCamera).setOnClickListener { _ ->
                    it.dismiss()
                    openCamera()
                }
            }
    }

    private fun openCamera() {
        ImagePicker.with(this)
            .crop()
            .cameraOnly()
            .compress(512)
            .maxResultSize(625, 625)
            .start()
    }

    private fun openGallery() {
        ImagePicker.with(this)
            .cropSquare()
            .galleryOnly()
            .galleryMimeTypes(
                mimeTypes = arrayOf(
                    "image/png",
                    "image/jpg",
                    "image/jpeg"
                )
            )
            .compress(512)
            .maxResultSize(625, 625)
            .start()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            val uri: Uri = data?.data!!

            binding.image = uri
            image = uri

        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onCheckPalindrome(text: String) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
    }

    override fun onSuccessSave(text: String) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
        goToActivity(ScreenTwoActivity::class.java,  null, clearIntent = true, isFinish = true)
    }

    override fun onFailedSave(text: String) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
    }

    override fun onGetProfile(profile: Profiles?) {
        if (profile != null) {
            println(profile.image)
            println(profile.name)
            binding.image = Uri.parse(profile.image)
            image = Uri.parse(profile.image)
            binding.etName.setText(profile.name)
        }
    }

    override fun onUpdateBaseUrlNeeded(type: String?, url: String?) {

    }

    override fun onUpdateTypeReceive(update: UpdateType?) {

    }

    override fun onGetVersion(version: String) {
        Toast.makeText(this, version, Toast.LENGTH_SHORT).show()
    }

    override fun onFetchVersionFailed(version: String) {

    }

}