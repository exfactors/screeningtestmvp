package suitmedia.com.dev.projectmvp.base.presenter

import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import kotlinx.coroutines.Job


/**
 * Created by Andri Dwi Utomo on 9/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
abstract class BasePresenter {
    fun addToLifeCycle(mvpView: MvpView?, job: Job?) {
        if (mvpView is LifecycleOwner) {
            (mvpView as LifecycleOwner).lifecycle.addObserver(object : DefaultLifecycleObserver {
                override fun onDestroy(owner: LifecycleOwner) {
                    owner.lifecycle.removeObserver(this)
                    job?.let{
                        job.cancel()
                    }
                    super.onDestroy(owner)
                }
            })
        }
    }
}