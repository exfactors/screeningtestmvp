package suitmedia.com.dev.projectmvp.feature.screenfour

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import suitmedia.com.dev.projectmvp.base.ui.adapter.BaseRecyclerAdapter
import suitmedia.com.dev.projectmvp.data.model.Users
import suitmedia.com.dev.projectmvp.databinding.ItemListEventBinding
import suitmedia.com.dev.projectmvp.databinding.ItemListGuestBinding
import suitmedia.com.dev.projectmvp.feature.screenthreefive.three.ListEventItemView


/**
 * Created by Andri Dwi Utomo on 10/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
class ListGuestAdapter(var context: Context) : BaseRecyclerAdapter<Users, ListGuestItemView>() {

    private lateinit var itemListGuestBinding: ItemListGuestBinding
    private var mOnActionListener: ListGuestItemView.OnActionListener? = null

    fun setOnActionListener(onActionListener: ListGuestItemView.OnActionListener) {
        mOnActionListener = onActionListener
    }

    var selectedItem = 0
        @SuppressLint("NotifyDataSetChanged")
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListGuestItemView {
        itemListGuestBinding = ItemListGuestBinding.inflate(LayoutInflater.from(context), parent, false)
        val view = ListGuestItemView(itemListGuestBinding)
        mOnActionListener?.let { view.setOnActionListener(it) }
        return view
    }

    override fun onBindViewHolder(holder: ListGuestItemView, position: Int) {
        context?.let { ctx ->
            /*if (position == selectedItem) {
                holder.getTitleView().setTextColor(ContextCompat.getColor(ctx, R.color.orange_primary))
            } else {
                holder.getTitleView().setTextColor(ContextCompat.getColor(ctx, R.color.black))
            }*/
        }
        super.onBindViewHolder(holder, position)
    }

}