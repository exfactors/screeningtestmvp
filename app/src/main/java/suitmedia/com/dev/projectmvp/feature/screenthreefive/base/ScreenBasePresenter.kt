package suitmedia.com.dev.projectmvp.feature.screenthreefive.base

import android.content.res.Resources
import suitmedia.com.dev.projectmvp.BaseApplication
import suitmedia.com.dev.projectmvp.R
import suitmedia.com.dev.projectmvp.base.presenter.BasePresenter
import suitmedia.com.dev.projectmvp.feature.screenone.ScreenOneView
import suitmedia.com.dev.projectmvp.feature.screenthreefive.five.ScreenFiveFragment
import suitmedia.com.dev.projectmvp.feature.screenthreefive.five.ScreenFiveView
import suitmedia.com.dev.projectmvp.feature.screenthreefive.three.ScreenThreeFragment

/**
 * Created by Andri Dwi Utomo on 10/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
class ScreenBasePresenter(val view: ScreenBaseView, val resources: Resources) : BasePresenter() {

    private var mvpView: ScreenBaseView? = null

    init {
        BaseApplication.applicationComponent.inject(this)
        declareView()
    }

    fun changeLayout(state: String) {
        if (state.equals("list")) {
            view.onChangeLayout("maps", ScreenThreeFragment(), resources.getDrawable(R.drawable.ic_baseline_map_24))
        } else {
            view.onChangeLayout("list", ScreenFiveFragment(), resources.getDrawable(R.drawable.ic_baseline_format_list_bulleted_24))
        }
    }

    private fun declareView() {
        mvpView = view
        addToLifeCycle(mvpView!!, null)
    }
}