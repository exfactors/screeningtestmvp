package suitmedia.com.dev.projectmvp.feature.screenthreefive.five

import android.content.Context
import com.google.gson.Gson
import suitmedia.com.dev.projectmvp.BaseApplication
import suitmedia.com.dev.projectmvp.base.presenter.BasePresenter
import suitmedia.com.dev.projectmvp.data.model.ListEvent
import suitmedia.com.dev.projectmvp.feature.screenthreefive.three.ScreenThreeView
import suitmedia.com.dev.projectmvp.helper.CommonUtils


/**
 * Created by Andri Dwi Utomo on 10/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
class ScreenFivePresenter(val context: Context, val view: ScreenFiveView) : BasePresenter() {
    private var mvpView: ScreenFiveView? = null

    init {
        BaseApplication.applicationComponent.inject(this)
        declareView()
    }

    fun loadEvent() {
        val response = CommonUtils.loadJSONFromAsset("event.json", context)
        val gSon = Gson()
        val arrayJson = gSon.fromJson(response, Array<ListEvent>::class.java)
        val listJson = arrayJson.toList()
        view.onLoadEvent(dataSelection(listJson))
    }

    private fun dataSelection(data: List<ListEvent>?) : List<ListEvent>{
        val dataFiltering: MutableList<ListEvent> = mutableListOf()
        data?.forEach { item ->
            dataFiltering.add(item)
        }

        return dataFiltering
    }

    private fun declareView() {
        mvpView = view
        addToLifeCycle(mvpView!!, null)
    }


}