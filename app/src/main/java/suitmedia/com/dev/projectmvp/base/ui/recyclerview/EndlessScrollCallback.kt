package suitmedia.com.dev.projectmvp.base.ui.recyclerview


/**
 * Created by Andri Dwi Utomo on 9/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
interface EndlessScrollCallback {
    fun loadMore()
}