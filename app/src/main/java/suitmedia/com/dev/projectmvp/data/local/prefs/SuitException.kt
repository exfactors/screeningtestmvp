package suitmedia.com.dev.projectmvp.data.local.prefs


/**
 * Created by Andri Dwi Utomo on 9/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
class SuitException(message: String?) : RuntimeException(message)