package suitmedia.com.dev.projectmvp.data.remote.services

import android.annotation.SuppressLint
import android.util.Log
import okhttp3.logging.HttpLoggingInterceptor


/**
 * Created by Andri Dwi Utomo on 9/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
class LoggingInterceptor  {
    companion object{
        private var SERVICE_TAG = "apiServices"

        @SuppressLint("LogNotTimber")
        private val loggingInterceptor = HttpLoggingInterceptor { message ->
            Log.d(
                SERVICE_TAG,
                message
            )
        }

        fun getLoggingInterceptor() : HttpLoggingInterceptor {
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            return loggingInterceptor
        }
    }
}