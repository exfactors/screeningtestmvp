package suitmedia.com.dev.projectmvp.data.remote.services

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import suitmedia.com.dev.projectmvp.data.local.prefs.DataConstant
import suitmedia.com.dev.projectmvp.data.local.prefs.SuitPreferences


/**
 * Created by Andri Dwi Utomo on 9/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
class NetworkInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request: Request = chain.request().newBuilder()
            .addHeader("Accept", "application/json")
            .addHeader(
                "Authorization",
                SuitPreferences.instance()?.getString(DataConstant.KEY_USER_TOKEN).toString()
            ).build()
        return chain.proceed(request)
    }
}