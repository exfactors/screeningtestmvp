package suitmedia.com.dev.projectmvp.data.model

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey


/**
 * Created by Andri Dwi Utomo on 9/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
open class User : RealmObject{

    @PrimaryKey
    @SerializedName("id")
    var id: Int? = 0

    @SerializedName("name")
    var name: String? = ""

    @SerializedName("email")
    var email: String? = ""

    @SerializedName("phone")
    var phone: String? = ""

}