package suitmedia.com.dev.projectmvp.base.ui.image.constant

/**
 * Created by Andri Dwi Utomo on 14/11/2021.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
enum class ImageProvider {
    GALLERY,
    CAMERA,
    BOTH
}