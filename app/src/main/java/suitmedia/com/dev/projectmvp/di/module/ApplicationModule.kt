package suitmedia.com.dev.projectmvp.di.module

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import suitmedia.com.dev.projectmvp.data.remote.services.APIService
import suitmedia.com.dev.projectmvp.data.remote.services.BaseServiceFactory
import suitmedia.com.dev.projectmvp.di.scope.SuitCoreApplicationScope


/**
 * Created by Andri Dwi Utomo on 9/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
@Module
class ApplicationModule(private val mApplication: Application) {

    @Provides
    internal fun provideApplication(): Application {
        return mApplication
    }

    @Provides
    @ApplicationContext
    internal fun provideContext(): Context {
        return mApplication
    }

    @Provides
    @SuitCoreApplicationScope
    internal fun provideAPIService(): APIService {
        return BaseServiceFactory.getAPIService()
    }
}