package suitmedia.com.dev.projectmvp.data.remote.services

import android.annotation.SuppressLint
import android.util.Log
import okhttp3.Interceptor
import okhttp3.Response
import suitmedia.com.dev.projectmvp.helper.CommonUtils


/**
 * Created by Andri Dwi Utomo on 9/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
class ErrorInterceptor : Interceptor {
    @SuppressLint("LogNotTimber")
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val response = chain.proceed(request)

        if (response.code() == 401) {
            Log.d("invalid_auth", response.code().toString())
        }

        return response
    }
}