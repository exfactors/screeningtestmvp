package suitmedia.com.dev.projectmvp.base.presenter


/**
 * Created by Andri Dwi Utomo on 9/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
interface MvpView {
    fun showDialogLoading(dismiss: Boolean = false, message: String?)
    fun showDialogAlert(title: String?, message: String?, confirmCallback: () -> Unit?={}, drawableImage: Int?=null)
    fun showDialogConfirmation(title: String?, message: String?, confirmCallback: () -> Unit?={}, cancelCallback: ()-> Unit? = {}, drawableImage: Int?=null)
    fun showDialogPopImage(drawableImage: Int?=null)
    fun hideLoading()
}