package suitmedia.com.dev.projectmvp.feature.screenthreefive.three

import android.widget.TextView
import suitmedia.com.dev.projectmvp.base.ui.adapter.viewholder.BaseItemViewHolder
import suitmedia.com.dev.projectmvp.data.model.ListEvent
import suitmedia.com.dev.projectmvp.databinding.ItemListEventBinding


/**
 * Created by Andri Dwi Utomo on 10/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
class ListEventItemView(var binding: ItemListEventBinding) : BaseItemViewHolder<ListEvent>(binding) {
    private var mActionListener: OnActionListener? = null
    private var listEvent: ListEvent? = null

    override fun bind(data: ListEvent?) {
        data?.let {
            listEvent = data
            binding.model = data

            binding.layGetEvent.setOnClickListener {
                if (mActionListener != null) {
                    mActionListener?.onClicked(this, adapterPosition - 1)
                }
            }
        }
    }

    fun getTitleView() : TextView {
        return binding.tvTitle
    }

    fun getData() : ListEvent? {
        return listEvent
    }

    fun setOnActionListener(listener: OnActionListener) {
        mActionListener = listener
    }

    interface OnActionListener {
        fun onClicked(view: ListEventItemView, position: Int)
    }

}