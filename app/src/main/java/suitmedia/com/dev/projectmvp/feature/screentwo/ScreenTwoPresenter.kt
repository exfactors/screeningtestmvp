package suitmedia.com.dev.projectmvp.feature.screentwo

import kotlinx.coroutines.CoroutineScope
import suitmedia.com.dev.projectmvp.BaseApplication
import suitmedia.com.dev.projectmvp.base.presenter.BasePresenter
import suitmedia.com.dev.projectmvp.data.local.LocalDataSource
import suitmedia.com.dev.projectmvp.feature.screenone.ScreenOneView


/**
 * Created by Andri Dwi Utomo on 10/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
class ScreenTwoPresenter(val view: ScreenTwoView) : BasePresenter() {

    private var localDataSource: LocalDataSource = LocalDataSource()

    private var mvpView: ScreenTwoView? = null

    init {
        BaseApplication.applicationComponent.inject(this)
        declareView()
    }
    fun getName() {
        val profile = localDataSource.getProfile()
        if (profile != null) {
            view.onGetName(profile!!.name)
        }
    }

    fun getEvent() {
        val event = localDataSource.getEvent()
        view.onGetEvent("Choose Event")
        println("isinya"+event)

        if (event != null) {
            view.onGetEvent(event!!.name)
        }

    }

    fun getGuest() {
        val guest = localDataSource.getGuest()
        view.onGetGuest("Choose Guest")
        if (guest != null) {
            view.onGetGuest(guest!!.name)
        }

    }

    private fun declareView() {
        mvpView = view
        addToLifeCycle(mvpView!!, null)
    }
}