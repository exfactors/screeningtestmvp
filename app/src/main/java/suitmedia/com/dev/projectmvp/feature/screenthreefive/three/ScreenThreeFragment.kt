package suitmedia.com.dev.projectmvp.feature.screenthreefive.three

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import suitmedia.com.dev.projectmvp.base.ui.BaseFragment
import suitmedia.com.dev.projectmvp.data.model.ListEvent
import suitmedia.com.dev.projectmvp.databinding.FragmentScreenThreeBinding
import suitmedia.com.dev.projectmvp.feature.screenthreefive.base.ScreenBaseActivity
import suitmedia.com.dev.projectmvp.feature.screentwo.ScreenTwoActivity


/**
 * Created by Andri Dwi Utomo on 10/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
class ScreenThreeFragment : BaseFragment<FragmentScreenThreeBinding>(), ScreenThreeView, ListEventItemView.OnActionListener {

    private var screenThreePresenter: ScreenThreePresenter? = null
    private var eventAdapter: ListEventAdapter? = null
    private var arrayEvent: List<ListEvent>? = emptyList()

    companion object {
        fun newInstance(): Fragment {
            return ScreenThreeFragment()
        }
    }

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentScreenThreeBinding.inflate(inflater, container, false)

    override fun onViewReady(savedInstanceState: Bundle?) {
        eventAdapter = ListEventAdapter(context)
        setupPresenter()
    }

    private fun setupPresenter() {
        screenThreePresenter = ScreenThreePresenter(requireContext(), this)
        screenThreePresenter!!.loadEvent()
    }

    override fun onLoadEvent(event: List<ListEvent>?) {
        event?.let {
            arrayEvent = event
            setupList(event)
            eventAdapter?.selectedItem = 0
        }
    }

    private fun setupList(event: List<ListEvent>) {
        binding.rvEvent.apply {
            setUpAsList()
            setAdapter(eventAdapter)
        }

        eventAdapter?.setOnActionListener(this)
        eventAdapter?.add(event)
        binding.rvEvent.stopShimmer()
        binding.rvEvent.showRecycler()
        finishLoad(binding.rvEvent)
    }

    override fun onSuccessSave(text: String) {
        goToActivity(ScreenTwoActivity::class.java,  null, clearIntent = true, isFinish = true)
    }

    override fun onClicked(view: ListEventItemView, position: Int) {
        view.getData().let { data ->
            screenThreePresenter!!.saveEvent(data?.title!!)
            Toast.makeText(requireContext(), data?.title, Toast.LENGTH_SHORT).show()
        }
    }
}