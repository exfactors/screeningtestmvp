package suitmedia.com.dev.projectmvp.di

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule


/**
 * Created by Andri Dwi Utomo on 6/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */

@GlideModule
class GlideAppModule : AppGlideModule() {
}