package suitmedia.com.dev.projectmvp.di.module

import javax.inject.Qualifier


/**
 * Created by Andri Dwi Utomo on 9/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
@Qualifier
@Retention annotation class ApplicationContext