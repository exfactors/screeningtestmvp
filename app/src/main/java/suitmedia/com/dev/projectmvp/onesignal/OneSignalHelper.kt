package suitmedia.com.dev.projectmvp.onesignal

import android.content.Context
import android.content.Intent
import com.onesignal.*
import suitmedia.com.dev.projectmvp.data.local.prefs.DataConstant
import suitmedia.com.dev.projectmvp.data.local.prefs.SuitPreferences
import suitmedia.com.dev.projectmvp.feature.screenone.ScreenOneActivity
import suitmedia.com.dev.projectmvp.helper.CommonConstant


/**
 * Created by Andri Dwi Utomo on 10/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
class OneSignalHelper {
    companion object {
        fun initOneSignal(context: Context){
            // OneSignal Initialization
            oneSignalLifeCycleHandler()
            OneSignal.initWithContext(context)
            OneSignal.setAppId(CommonConstant.ONE_SIGNAL_APP_ID)

            initNotificationOpenedHandler(context)
            notificationForeGroundHandler(context)

            OneSignal.unsubscribeWhenNotificationsAreDisabled(true)
            OneSignal.pauseInAppMessages(true)
            OneSignal.setLocationShared(false)

            val device = OneSignal.getDeviceState()
            val userId = device?.userId //push player_id

            userId?.let {
                SuitPreferences.instance()?.saveString(DataConstant.PLAYER_ID, it)
            }

            if (BuildConfig.DEBUG) {
                OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE)
            }
        }

        private fun initNotificationOpenedHandler(context: Context) {
            OneSignal.setNotificationOpenedHandler { result: OSNotificationOpenedResult ->
                OneSignal.onesignalLog(
                    OneSignal.LOG_LEVEL.VERBOSE,
                    "OSNotificationOpenedResult result: $result"
                )
                goToActivity(context)
            }
        }

        private fun notificationForeGroundHandler(context: Context){
            OneSignal.setNotificationWillShowInForegroundHandler { notificationReceivedEvent: OSNotificationReceivedEvent ->
                OneSignal.onesignalLog(
                    OneSignal.LOG_LEVEL.VERBOSE, "NotificationWillShowInForegroundHandler fired!" +
                            " with notification event: " + notificationReceivedEvent.toString()
                )
                val notification = notificationReceivedEvent.notification
                //val data = notification.additionalData
                notificationReceivedEvent.complete(notification)
                goToActivity(context)
            }
        }

        private fun oneSignalLifeCycleHandler(){

            OneSignal.setInAppMessageLifecycleHandler(object : OSInAppMessageLifecycleHandler(){
                override fun onWillDisplayInAppMessage(message: OSInAppMessage) {
                    OneSignal.onesignalLog(
                        OneSignal.LOG_LEVEL.VERBOSE,
                        "MainApplication onWillDisplayInAppMessage"
                    )
                }

                override fun onDidDisplayInAppMessage(message: OSInAppMessage) {
                    OneSignal.onesignalLog(
                        OneSignal.LOG_LEVEL.VERBOSE,
                        "MainApplication onDidDisplayInAppMessage"
                    )
                }

                override fun onWillDismissInAppMessage(message: OSInAppMessage) {
                    OneSignal.onesignalLog(
                        OneSignal.LOG_LEVEL.VERBOSE,
                        "MainApplication onWillDismissInAppMessage"
                    )
                }

                override fun onDidDismissInAppMessage(message: OSInAppMessage) {
                    OneSignal.onesignalLog(
                        OneSignal.LOG_LEVEL.VERBOSE,
                        "MainApplication onDidDismissInAppMessage"
                    )
                }
            })

        }

        private fun goToActivity(context: Context){
            val intent = Intent()
            intent.setClass(context, ScreenOneActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }
    }
}