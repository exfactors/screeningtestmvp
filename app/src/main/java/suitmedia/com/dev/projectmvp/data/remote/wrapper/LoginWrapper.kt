package suitmedia.com.dev.projectmvp.data.remote.wrapper

import com.google.gson.annotations.SerializedName
import suitmedia.com.dev.projectmvp.data.model.User


/**
 * Created by Andri Dwi Utomo on 9/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
open class LoginWrapper {
    @SerializedName("type")
    var type: String? = ""

    @SerializedName("token")
    var token: String? = ""

    @SerializedName("user")
    var user: User? = null
}