package suitmedia.com.dev.projectmvp.data.local

import io.realm.*
import io.realm.query.RealmQuery
import io.realm.schema.RealmSchema
import suitmedia.com.dev.projectmvp.data.model.Event
import suitmedia.com.dev.projectmvp.data.model.Guest
import suitmedia.com.dev.projectmvp.data.model.GuestApi
import suitmedia.com.dev.projectmvp.data.model.Profiles


/**
 * Created by Andri Dwi Utomo on 9/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
class RealmHelper<T : RealmObject> {
    val config = RealmConfiguration.Builder(schema = setOf(Profiles::class, Event::class, Guest::class, GuestApi::class))
        .schemaVersion(1)
        .deleteRealmIfMigrationNeeded()
        .build()
    val realm: Realm = Realm.open(config)

    /*fun saveSingleObject(data: T) {
        try {
            realm.writeBlocking {
                val all: RealmResults<T> = this.query<T>().find()
                // call delete on the results of a query to delete those objects permanently
                delete(all)

                this.copyToRealm(data)
            }
        } finally {
            realm?.close()
        }
    }*/
}