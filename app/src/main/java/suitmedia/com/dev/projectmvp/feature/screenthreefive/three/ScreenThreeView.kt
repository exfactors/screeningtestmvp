package suitmedia.com.dev.projectmvp.feature.screenthreefive.three

import suitmedia.com.dev.projectmvp.base.presenter.MvpView
import suitmedia.com.dev.projectmvp.data.model.ListEvent

/**
 * Created by Andri Dwi Utomo on 10/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
interface ScreenThreeView : MvpView {
    fun onLoadEvent(event: List<ListEvent>?)
    fun onSuccessSave(text: String)
}