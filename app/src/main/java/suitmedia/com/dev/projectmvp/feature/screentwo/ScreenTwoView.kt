package suitmedia.com.dev.projectmvp.feature.screentwo

import android.annotation.SuppressLint
import suitmedia.com.dev.projectmvp.base.presenter.MvpView


/**
 * Created by Andri Dwi Utomo on 10/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
@SuppressLint("CustomScreenTwo")
interface ScreenTwoView : MvpView {
    fun onGetName(name: String)
    fun onGetEvent(name: String)
    fun onGetGuest(name: String)
}