package suitmedia.com.dev.projectmvp.data.local.prefs


/**
 * Created by Andri Dwi Utomo on 9/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
object DataConstant {
    const val KEY_USER_TOKEN = "user_token"
    const val KEY_MEMBER_CACHE = "member_list"
    const val RANDOM_KEY = "random_key"
    const val PLAYER_ID = "player_id"
    const val BASE_URL = "base_url"
    const val IS_LOGIN = "is_login"
    const val CURRENT_LANG = "en"
}