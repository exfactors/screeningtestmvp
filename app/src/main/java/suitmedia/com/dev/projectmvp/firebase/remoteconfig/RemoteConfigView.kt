package suitmedia.com.dev.projectmvp.firebase.remoteconfig

import suitmedia.com.dev.projectmvp.base.presenter.MvpView
import suitmedia.com.dev.projectmvp.data.model.UpdateType


/**
 * Created by Andri Dwi Utomo on 9/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
interface RemoteConfigView : MvpView {

    fun onUpdateBaseUrlNeeded(type: String?, url: String?)

    fun onUpdateTypeReceive(update: UpdateType?)

    fun onGetVersion(version: String)

    fun onFetchVersionFailed(version: String)

}