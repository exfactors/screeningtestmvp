package suitmedia.com.dev.projectmvp.feature.screenone

import android.annotation.SuppressLint
import suitmedia.com.dev.projectmvp.base.presenter.MvpView
import suitmedia.com.dev.projectmvp.data.model.Profiles


/**
 * Created by Andri Dwi Utomo on 9/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
@SuppressLint("CustomScreenOne")
interface ScreenOneView : MvpView {
    fun onCheckPalindrome(text: String)

    fun onSuccessSave(text: String)

    fun onFailedSave(text: String)

    fun onGetProfile(profiles: Profiles?)
}