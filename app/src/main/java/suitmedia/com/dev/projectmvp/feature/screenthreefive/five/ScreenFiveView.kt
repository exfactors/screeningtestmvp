package suitmedia.com.dev.projectmvp.feature.screenthreefive.five

import suitmedia.com.dev.projectmvp.base.presenter.MvpView
import suitmedia.com.dev.projectmvp.data.model.ListEvent

/**
 * Created by Andri Dwi Utomo on 10/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
interface ScreenFiveView : MvpView {
    fun onLoadEvent(event: List<ListEvent>)
}