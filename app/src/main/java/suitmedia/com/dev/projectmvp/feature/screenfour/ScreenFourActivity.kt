package suitmedia.com.dev.projectmvp.feature.screenfour

import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import suitmedia.com.dev.projectmvp.base.ui.BaseActivity
import suitmedia.com.dev.projectmvp.data.model.ListEvent
import suitmedia.com.dev.projectmvp.data.model.Users
import suitmedia.com.dev.projectmvp.databinding.ActivityScreenFourBinding
import suitmedia.com.dev.projectmvp.feature.screenthreefive.three.ListEventAdapter
import suitmedia.com.dev.projectmvp.feature.screentwo.ScreenTwoActivity


/**
 * Created by Andri Dwi Utomo on 10/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
class ScreenFourActivity : BaseActivity<ActivityScreenFourBinding>(), ScreenFourView, ListGuestItemView.OnActionListener {
    private var screenFourPresenter: ScreenFourPresenter? = null
    private var guestAdapter: ListGuestAdapter? = null
    private var arrayGuest: List<Users>? = emptyList()

    var page = 1
    var showpage = 1
    var perpage = 10
    var total = 0

    override fun getViewBinding() = ActivityScreenFourBinding.inflate(layoutInflater)

    override fun onViewReady(savedInstanceState: Bundle?) {
        binding.toolbar.tvToolbarTitle.text = "Guest"
        binding.toolbar.ivToolbarIconLeft.setOnClickListener {
            goToActivity(ScreenTwoActivity::class.java,  null, clearIntent = true, isFinish = true)
        }
        setupPresenter()
        guestAdapter = ListGuestAdapter(this)
        actionList()
    }

    private fun setupPresenter() {
        screenFourPresenter = ScreenFourPresenter(this)
        screenFourPresenter?.getGuestApi(page, perpage)
    }

    private fun actionList() {
        binding.rvGuest.baseRecyclerBinding.swipeRefresh.setOnRefreshListener {
            binding.rvGuest.baseRecyclerBinding.swipeRefresh.isRefreshing = false
            perpage = 10
            screenFourPresenter!!.getGuestApi(page, perpage)
        }

        binding.rvGuest.baseRecyclerBinding.recyclerView.addOnScrollListener(scrollToPosition())
        /*binding.rvGuest.addOnScrollListener(scrollToPosition())*/
    }

    private fun setupList(guest: List<Users>) {
        binding.rvGuest.apply {
            setUpAsGrid(2)
            setAdapter(guestAdapter)
        }
        guestAdapter?.clear()
        guestAdapter?.setOnActionListener(this)
        guestAdapter?.add(guest)
        binding.rvGuest.stopShimmer()
        binding.rvGuest.showRecycler()
        finishLoad(binding.rvGuest)

    }

    override fun onGetGuest(guests: List<Users>, page: Int) {
        guests?.let {
            arrayGuest = guests
            setupList(guests)

            guestAdapter?.selectedItem = 0
        }
        total = page
    }

    override fun onError(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun onSuccessSave(text: String) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
        goToActivity(ScreenTwoActivity::class.java,  null, clearIntent = true, isFinish = true)

    }

    override fun onClicked(view: ListGuestItemView, position: Int) {
        view.getData().let { data ->
            Toast.makeText(this, data?.firstName, Toast.LENGTH_SHORT).show()
            screenFourPresenter!!.saveGuest(data?.firstName!!)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        goToActivity(ScreenTwoActivity::class.java,  null, clearIntent = true, isFinish = true)
    }

    private fun scrollToPosition() = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
            if (!recyclerView.canScrollVertically(1) && newState == RecyclerView.SCROLL_STATE_IDLE) {
                //reached end
                if (perpage < total) {
                    showpage++
                    perpage = showpage * perpage
                    Toast.makeText(getActivity(), "Loading Data...", Toast.LENGTH_SHORT).show()
                    screenFourPresenter!!.getGuestApi(page, perpage)

                }


            }

            if (!recyclerView.canScrollVertically(-1) && newState == RecyclerView.SCROLL_STATE_IDLE) {
                //reached top
            }
            if(newState == RecyclerView.SCROLL_STATE_DRAGGING){
                //scrolling

            }
        }
    }

}