package suitmedia.com.dev.projectmvp.data.model

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject


/**
 * Created by Andri Dwi Utomo on 13/5/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
open class Responses {
    @SerializedName("page")
    val page: Int? = 0

    @SerializedName("per_page")
    val perPage: Int? = 0

    @SerializedName("total")
    val total: Int? = 0

    @SerializedName("total_pages")
    val totalPage: Int? = 0

    @SerializedName("data")
    val data: List<Users>? = null
}

open class Users {
    @SerializedName("id")
    val id: Int? = 0

    @SerializedName("email")
    val email: String? = ""

    @SerializedName("first_name")
    val firstName: String? = ""

    @SerializedName("last_name")
    val lastName: String? = ""

    @SerializedName("avatar")
    val avatar: String? = ""
}
