package suitmedia.com.dev.projectmvp.helper

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.telephony.TelephonyManager
import org.json.JSONObject
import retrofit2.HttpException
import suitmedia.com.dev.projectmvp.BuildConfig
import suitmedia.com.dev.projectmvp.data.local.prefs.DataConstant
import suitmedia.com.dev.projectmvp.data.local.prefs.SuitPreferences
import java.io.IOException


/**
 * Created by Andri Dwi Utomo on 9/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
class CommonUtils {

    companion object {

        fun openAppInStore(context: Context) {
            // you can also use BuildConfig.APPLICATION_ID
            try {
                val appId = context.packageName
                val rateIntent = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=$appId"))
                var marketFound = false
                // find all applications able to handle our rateIntent
                val otherApps = context.packageManager
                    .queryIntentActivities(rateIntent, 0)
                for (otherApp in otherApps) {
                    // look for Google Play application
                    if (otherApp.activityInfo.applicationInfo.packageName == "com.android.vending") {

                        val otherAppActivity = otherApp.activityInfo
                        val componentName = ComponentName(
                            otherAppActivity.applicationInfo.packageName,
                            otherAppActivity.name
                        )
                        // make sure it does NOT open in the stack of your activity
                        rateIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        // task reparenting if needed
                        rateIntent.addFlags(Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED)
                        // if the Google Play was already open in a search result
                        //  this make sure it still go to the app page you requested
                        rateIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        // this make sure only the Google Play app is allowed to
                        // intercept the intent
                        rateIntent.component = componentName
                        context.startActivity(rateIntent)
                        marketFound = true
                        break

                    }
                }

                // if GP not present on device, open web browser
                if (!marketFound) {
                    val webIntent = Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/details?id=$appId"))
                    context.startActivity(webIntent)

                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }


        fun createIntent(context: Context, actDestination: Class<out Activity>): Intent {
            return Intent(context, actDestination)
        }

        fun clearLocalStorage() {
            val suitPreferences = SuitPreferences.instance()
            val currentUrl: String? = SuitPreferences.instance()?.getString(DataConstant.BASE_URL)
            suitPreferences?.clearSession()
            currentUrl?.let{
                SuitPreferences.instance()?.saveString(DataConstant.BASE_URL, currentUrl.toString())
            }
            /*val realm: RealmHelper<User> = RealmHelper()
            realm.removeAllData()*/
        }

        fun setDefaultBaseUrlIfNeeded() {
            val currentUrl: String? = SuitPreferences.instance()?.getString(DataConstant.BASE_URL)
            if (currentUrl == null) {
                SuitPreferences.instance()?.saveString(DataConstant.BASE_URL, BuildConfig.API_URL)
            }
        }

        fun loadJSONFromAsset(json_name: String, context: Context): String? {
            val json: String
            try {
                val `is` = context.assets.open(json_name)
                val size = `is`.available()
                val buffer = ByteArray(size)
                `is`.read(buffer)
                `is`.close()
                json = String(buffer, charset("UTF-8"))
            } catch (ex: IOException) {
                ex.printStackTrace()
                return null
            }

            return json
        }

        fun convertData(type: String?): CommonConstant.UpdateMode{
            return when(type){
                "flexible" -> CommonConstant.UpdateMode.FLEXIBLE
                "immediate" -> CommonConstant.UpdateMode.IMMEDIATE
                else -> CommonConstant.UpdateMode.FLEXIBLE
            }
        }

        fun isUpdateAvailable(version: Int?): Boolean{
            version?.let{
                val currentVersion = BuildConfig.VERSION_CODE
                return version != 0 && currentVersion < it
            }?:run{
                return false
            }
        }

        fun getErrorResponse(throwable: Throwable): String {
            if (throwable is HttpException) {
                val jsonData: String = throwable.response()?.errorBody()?.string().toString()
                if (jsonData.contains("status")) {
                    val o = JSONObject(jsonData)
                    var word = o.get("message").toString()
                    try {
                        val jo = JSONObject(o.get("errors").toString())
                        word = jo.toString().substring(
                            jo.toString().indexOf("[\"").plus(2),
                            jo.toString().indexOf("\"]")
                        )
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    return word
                } else {
                    return throwable.message()
                }
            } else {
                return throwable.message.toString()
            }
        }

    }

}