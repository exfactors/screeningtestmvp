package suitmedia.com.dev.projectmvp.feature.screenfour

import suitmedia.com.dev.projectmvp.base.presenter.MvpView
import suitmedia.com.dev.projectmvp.data.model.Users


/**
 * Created by Andri Dwi Utomo on 10/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
interface ScreenFourView : MvpView {
    fun onGetGuest(guests: List<Users>, page: Int)
    fun onError(message: String)
    fun onSuccessSave(text: String)
}