package suitmedia.com.dev.projectmvp.data.model

import com.google.gson.annotations.SerializedName


/**
 * Created by Andri Dwi Utomo on 9/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
open class UpdateType {

    @SerializedName("update_type")
    var updateType: String? = ""
    @SerializedName("category")
    var category: String? = ""
    @SerializedName("latest_vcode")
    var latestVersionCode: Int? = 0
    @SerializedName("messages")
    var messages: String? = ""

}