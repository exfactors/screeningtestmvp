package suitmedia.com.dev.projectmvp.firebase.remoteconfig

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import com.google.gson.Gson
import suitmedia.com.dev.projectmvp.BaseApplication
import suitmedia.com.dev.projectmvp.BuildConfig
import suitmedia.com.dev.projectmvp.R
import suitmedia.com.dev.projectmvp.base.presenter.BasePresenter
import suitmedia.com.dev.projectmvp.data.local.prefs.DataConstant
import suitmedia.com.dev.projectmvp.data.local.prefs.SuitPreferences
import suitmedia.com.dev.projectmvp.data.model.UpdateType
import suitmedia.com.dev.projectmvp.helper.CommonConstant
import suitmedia.com.dev.projectmvp.helper.CommonUtils


/**
 * Created by Andri Dwi Utomo on 9/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
class RemoteConfigPresenter(var view: RemoteConfigView) : BasePresenter() {

    private var mFireBaseRemoteConfig: FirebaseRemoteConfig? = null
    private var mvpView: RemoteConfigView? = null

    init {
        BaseApplication.applicationComponent.inject(this)
        declareView()
        setupFireBaseRemoteConfig()
    }

    private fun setupFireBaseRemoteConfig() {
        mFireBaseRemoteConfig = FirebaseRemoteConfig.getInstance()
        mFireBaseRemoteConfig?.setConfigSettingsAsync(
            FirebaseRemoteConfigSettings.Builder().setMinimumFetchIntervalInSeconds(0)
                .build())

        mFireBaseRemoteConfig?.setDefaultsAsync(R.xml.version_app)
    }

    fun getVersion() {
        if (mFireBaseRemoteConfig == null) {
            setupFireBaseRemoteConfig()
        }
        mFireBaseRemoteConfig?.fetchAndActivate()
            ?.addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val updated = task.result
                    Log.d("fetch remote", "Config params updated: $updated")
                    val version = mFireBaseRemoteConfig!!.getString("version_app")
                    mvpView?.onGetVersion(version)
                } else {
                    mvpView?.onFetchVersionFailed("Fetch Failed")
                }
            }
    }

    fun checkBaseUrl() {
        if (mFireBaseRemoteConfig == null) {
            setupFireBaseRemoteConfig()
        }

        mFireBaseRemoteConfig?.fetchAndActivate()
            ?.addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val updated = task.result
                    Log.d("***", "Config params updated: $updated")
                }

                val newBaseUrl: String? = mFireBaseRemoteConfig?.getString(CommonConstant.NEW_BASE_URL)
                val currentUrl: String? = SuitPreferences.instance()?.getString(DataConstant.BASE_URL)
                if (newBaseUrl != null) {
                    if (newBaseUrl.toString().isNotEmpty()) {
                        if (currentUrl != newBaseUrl) mvpView?.onUpdateBaseUrlNeeded("new", newBaseUrl.toString())
                    } else {
                        if (currentUrl != null && currentUrl.isNotEmpty() && currentUrl != BuildConfig.API_URL) {
                            mvpView?.onUpdateBaseUrlNeeded("default", BuildConfig.API_URL)
                        } else {
                            CommonUtils.setDefaultBaseUrlIfNeeded()
                        }
                    }
                }
            }
    }

    fun getUpdateType(context: Context) {
        var updateFromConsole: String? = ""
        val updateDefaultJSON = CommonUtils.loadJSONFromAsset("update.json", context)
        val gSon = Gson()
        var updateType: UpdateType? = gSon.fromJson(updateDefaultJSON, UpdateType::class.java)

        if (mFireBaseRemoteConfig == null) {
            setupFireBaseRemoteConfig()
        }

        mFireBaseRemoteConfig?.fetchAndActivate()
            ?.addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val updated = task.result
                    Log.d("***", "Config params updated: $updated")
                }

                if (mFireBaseRemoteConfig?.getString(CommonConstant.NOTIFY_UPDATE_TYPE) != null && mFireBaseRemoteConfig?.getString(CommonConstant.NOTIFY_UPDATE_TYPE)!!.isNotEmpty()) {
                    updateFromConsole = mFireBaseRemoteConfig?.getString(CommonConstant.NOTIFY_UPDATE_TYPE)
                    updateType = UpdateType()
                    updateType = gSon.fromJson(updateFromConsole.toString(), UpdateType::class.java)
                    mvpView?.onUpdateTypeReceive(updateType)
                }else{
                    mvpView?.onUpdateTypeReceive(updateType)
                }
            }
    }

    private fun declareView(){
        mvpView = view
        addToLifeCycle(mvpView!!, null)
    }

}