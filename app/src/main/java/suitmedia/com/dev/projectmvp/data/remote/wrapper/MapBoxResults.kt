package suitmedia.com.dev.projectmvp.data.remote.wrapper

import com.google.gson.annotations.SerializedName


/**
 * Created by Andri Dwi Utomo on 9/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
class MapBoxResults<T> {


    @SerializedName("features")
    var arrayData: List<T>? = null
}