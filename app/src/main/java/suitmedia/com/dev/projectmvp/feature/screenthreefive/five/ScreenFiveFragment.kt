package suitmedia.com.dev.projectmvp.feature.screenthreefive.five

import android.Manifest
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener
import suitmedia.com.dev.projectmvp.R
import suitmedia.com.dev.projectmvp.base.ui.BaseFragment
import suitmedia.com.dev.projectmvp.base.ui.extension.isLocationPermissionGranted
import suitmedia.com.dev.projectmvp.data.model.ListEvent
import suitmedia.com.dev.projectmvp.databinding.FragmentScreenFiveBinding


/**
 * Created by Andri Dwi Utomo on 10/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
class ScreenFiveFragment : BaseFragment<FragmentScreenFiveBinding>(), ScreenFiveView, OnMapReadyCallback {

    private var screenFivePresenter: ScreenFivePresenter? = null
    private var arrayEvent: List<ListEvent>? = emptyList()

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentScreenFiveBinding.inflate(inflater, container, false)

    companion object {
        const val TAG = "OpenMapDialog"
        private const val REQ_PERMISSION_LOCATION = 1001
    }

    private lateinit var mMap: GoogleMap

    override fun onViewReady(savedInstanceState: Bundle?) {
        setupPresenter()

        requestLocationPermission()

        val supportMapFragment = childFragmentManager.findFragmentById(R.id.mapView) as SupportMapFragment
        supportMapFragment!!.getMapAsync(this)
    }

    private fun setupPresenter() {
        screenFivePresenter = ScreenFivePresenter(requireContext(), this)
        screenFivePresenter!!.loadEvent()
    }

    override fun onLoadEvent(event: List<ListEvent>) {
        arrayEvent = event
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mapEvent(arrayEvent!!, 0.0,0.0)
    }

    private fun mapEvent(lEvent: List<ListEvent>, lat: Double, long: Double) {

        mMap.clear()

        lEvent.mapIndexed { i, event ->
            val point = LatLng(event.latitude!!, event.longitude!!)
            val marker = MarkerOptions()
            marker.position(point)
            marker.title(event.title)
            if (lat == 0.0 && long == 0.0) {
                if (i == 0) {
                    marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_selected))
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(point, 15F))
                    binding.tvTitle.text = event.title
                    binding.tvDesc.text = event.description
                    binding.tvDate.text = event.date
                    binding.tvTime.text = event.time
                    binding.image = Uri.parse(event.image)
                } else {
                    marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_unselected))
                }
            } else {
                if (lat == event.latitude && long == event.longitude) {
                    marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_selected))
                } else {
                    marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_unselected))
                }

            }

            mMap.addMarker(marker)

            mMap.setOnMarkerClickListener(OnMarkerClickListener { marker ->

                var getEvent = lEvent.find { it.latitude == marker.position.latitude && it.longitude == marker.position.longitude }
                binding.tvTitle.text = getEvent?.title
                binding.tvDesc.text = getEvent?.description
                binding.tvDate.text = getEvent?.date
                binding.tvTime.text = getEvent?.time
                binding.image = Uri.parse(getEvent?.image)
                mapEvent(lEvent, marker.position.latitude, marker.position.longitude)
                false
            })
        }
    }

    private fun requestLocationPermission() {
        if (!requireContext().isLocationPermissionGranted()) {
            val relationalFine = ActivityCompat.shouldShowRequestPermissionRationale(
                requireActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            val relationalCoarse = ActivityCompat.shouldShowRequestPermissionRationale(
                requireActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
            if (relationalFine || relationalCoarse) {
                Toast.makeText(requireContext(), "Location permission needed", Toast.LENGTH_SHORT).show()
            }
            requestPermissions(
                arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ),
                ScreenFiveFragment.REQ_PERMISSION_LOCATION
            )
        }
    }
}