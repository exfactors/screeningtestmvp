package suitmedia.com.dev.projectmvp.feature.screenthreefive.base

import android.graphics.drawable.Drawable
import androidx.fragment.app.Fragment
import suitmedia.com.dev.projectmvp.base.presenter.MvpView


/**
 * Created by Andri Dwi Utomo on 10/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
interface ScreenBaseView : MvpView {
    fun onChangeLayout(state: String, fragment: Fragment, drawable: Drawable)

}