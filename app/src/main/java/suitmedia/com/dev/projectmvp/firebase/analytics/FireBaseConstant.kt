package suitmedia.com.dev.projectmvp.firebase.analytics


/**
 * Created by Andri Dwi Utomo on 9/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
object FireBaseConstant {
    // Screen View Tracking
    const val SCREEN_LOGIN = "Login"
    const val SCREEN_MEMBER = "Banner"

    // Event Tracking
    const val EVENT_SKIP_LOGIN = "SkipLogin"
}