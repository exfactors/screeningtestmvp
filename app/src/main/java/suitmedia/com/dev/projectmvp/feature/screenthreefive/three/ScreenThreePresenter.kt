package suitmedia.com.dev.projectmvp.feature.screenthreefive.three

import android.content.Context
import com.google.gson.Gson
import kotlinx.coroutines.*
import suitmedia.com.dev.projectmvp.BaseApplication
import suitmedia.com.dev.projectmvp.base.presenter.BasePresenter
import suitmedia.com.dev.projectmvp.data.local.LocalDataSource
import suitmedia.com.dev.projectmvp.data.model.ListEvent
import suitmedia.com.dev.projectmvp.feature.screenthreefive.base.ScreenBaseView
import suitmedia.com.dev.projectmvp.helper.CommonUtils
import kotlin.coroutines.CoroutineContext


/**
 * Created by Andri Dwi Utomo on 10/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
class ScreenThreePresenter(val context: Context, val view: ScreenThreeView) : BasePresenter(), CoroutineScope {
    private val job = Job()
    override val coroutineContext: CoroutineContext = job + Dispatchers.IO

    private var localDataSource: LocalDataSource = LocalDataSource()
    private var mvpView: ScreenThreeView? = null


    init {
        BaseApplication.applicationComponent.inject(this)
        declareView()
    }

    fun loadEvent() {
        val response = CommonUtils.loadJSONFromAsset("event.json", context)
        val gSon = Gson()
        val arrayJson = gSon.fromJson(response, Array<ListEvent>::class.java)
        val listJson = arrayJson.toList()
        view.onLoadEvent(dataSelection(listJson))
    }

    private fun dataSelection(data: List<ListEvent>?) : List<ListEvent>{
        val dataFiltering: MutableList<ListEvent> = mutableListOf()
        data?.forEach { item ->
            dataFiltering.add(item)
        }

        return dataFiltering
    }

    fun saveEvent(name: String) {
        println("namanya"+name)
        launch {
            withContext(Dispatchers.Main) {
                println(localDataSource.addEvent(name))
                view.onSuccessSave("Success")
            }
        }
    }

    private fun declareView() {
        mvpView = view
        addToLifeCycle(mvpView!!, job)
    }
}