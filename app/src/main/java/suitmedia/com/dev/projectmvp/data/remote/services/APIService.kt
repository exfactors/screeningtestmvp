package suitmedia.com.dev.projectmvp.data.remote.services

import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.http.*
import suitmedia.com.dev.projectmvp.data.model.Responses
import suitmedia.com.dev.projectmvp.data.model.User
import suitmedia.com.dev.projectmvp.data.model.Users
import suitmedia.com.dev.projectmvp.data.remote.wrapper.Results


/**
 * Created by Andri Dwi Utomo on 9/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
interface APIService {

    /*@GET("users")
    fun getMembers(
        @Query("per_page") perPage: Int,
        @Query("page") page: Int
    ): Single<Results<User>>*/

    @GET("users")
    fun getMembersCoroutinesAsync(
        @Query("page") page: Int,
        @Query("per_page") perPage: Int
    ): Call<Results<Users>>

    /*@GET
    fun searchPlaceAsync(@Url url: String?): Deferred<MapBoxResults<Place>>

    @POST("auth/login")
    fun login(@Body body: LoginRequest): Deferred<ResultObject<LoginWrapper>>

    @FormUrlEncoded
    @POST("login")
    fun login2(
        @Field("email") email: String,
        @Field("password") password: String
    ): Single<ResultObject<LoginWrapper>>


    @GET("banners")
    fun getBanners(
        @Query("page[number]") page: Int = 1,
        @Query("page[size]") perPage: Int = 10
    ): Deferred<ResultList<Banner>>*/

}
