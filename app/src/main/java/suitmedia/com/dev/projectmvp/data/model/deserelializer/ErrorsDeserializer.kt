package suitmedia.com.dev.projectmvp.data.model.deserelializer

import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonElement
import com.google.gson.reflect.TypeToken
import org.json.JSONException
import org.json.JSONObject
import suitmedia.com.dev.projectmvp.data.remote.wrapper.ChildError
import suitmedia.com.dev.projectmvp.data.remote.wrapper.Errors
import java.lang.reflect.Type


/**
 * Created by Andri Dwi Utomo on 9/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
class ErrorsDeserializer: SuitCoreJsonDeserializer<Errors>() {
    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): Errors {
        val error = Errors()
        val jsonObject = json?.asJsonObject
        val errors = jsonObject?.getAsJsonObject("errors")
        val errorList: MutableList<ChildError> = mutableListOf()
        try {
            val `object` = JSONObject(errors.toString())
            val iteration = `object`.keys()
            while (iteration.hasNext()) {
                val key = iteration.next()
                try {
                    val value = `object`.getJSONArray(key)
                    val objResponse: Array<String> = Gson().fromJson(
                        value.toString(),
                        Array<String>::class.java
                    )
                    val type: Type = object : TypeToken<List<String>?>() {}.type

                    val data: List<String> = Gson().fromJson(objResponse.toString(), type)
                    val mapTemp: MutableMap<String, List<String>> = mutableMapOf()
                    val child: ChildError = ChildError()
                    mapTemp[key] = data
                    child.data = mapTemp
                    errorList.add(child)

                } catch (e: JSONException) {
                    Log.e("Deserializer", "deserialize error: ", e)
                }
            }
            error.parentList = errorList
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        return error
    }
}