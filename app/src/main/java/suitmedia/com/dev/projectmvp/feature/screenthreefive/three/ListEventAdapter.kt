package suitmedia.com.dev.projectmvp.feature.screenthreefive.three

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import suitmedia.com.dev.projectmvp.R
import suitmedia.com.dev.projectmvp.base.ui.adapter.BaseRecyclerAdapter
import suitmedia.com.dev.projectmvp.data.model.ListEvent
import suitmedia.com.dev.projectmvp.databinding.ItemListEventBinding


/**
 * Created by Andri Dwi Utomo on 10/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
class ListEventAdapter(var context: Context?) : BaseRecyclerAdapter<ListEvent, ListEventItemView>() {

    private lateinit var itemListEventBinding: ItemListEventBinding
    private var mOnActionListener: ListEventItemView.OnActionListener? = null

    fun setOnActionListener(onActionListener: ListEventItemView.OnActionListener) {
        mOnActionListener = onActionListener
    }

    var selectedItem = 0
    @SuppressLint("NotifyDataSetChanged")
    set(value) {
        field = value
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListEventItemView {
        itemListEventBinding = ItemListEventBinding.inflate(LayoutInflater.from(context), parent, false)
        val view = ListEventItemView(itemListEventBinding)
        mOnActionListener?.let { view.setOnActionListener(it) }
        return view
    }

    override fun onBindViewHolder(holder: ListEventItemView, position: Int) {
        context?.let { ctx ->
            /*if (position == selectedItem) {
                holder.getTitleView().setTextColor(ContextCompat.getColor(ctx, R.color.orange_primary))
            } else {
                holder.getTitleView().setTextColor(ContextCompat.getColor(ctx, R.color.black))
            }*/
        }
        super.onBindViewHolder(holder, position)
    }

}