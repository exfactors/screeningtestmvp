package suitmedia.com.dev.projectmvp.firebase.remoteconfig

import android.app.Activity
import android.widget.Toast
import suitmedia.com.dev.projectmvp.data.local.prefs.DataConstant
import suitmedia.com.dev.projectmvp.data.local.prefs.SuitPreferences
import suitmedia.com.dev.projectmvp.helper.CommonUtils
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit


/**
 * Created by Andri Dwi Utomo on 9/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
class RemoteConfigHelper {

    companion object {

        fun changeBaseUrl(activity: Activity?, type: String, endpoint: String) {
            var url = ""
            CommonUtils.clearLocalStorage()

            when (type) {
                "new" -> url = endpoint
                "default" -> url = endpoint
            }
            SuitPreferences.instance()?.saveString(DataConstant.BASE_URL, url)

            Executors.newSingleThreadScheduledExecutor().schedule({
                /*CommonUtils.restartApp(activity)*/
            }, 1, TimeUnit.SECONDS)

        }

        fun getVersion() {

        }
    }
}