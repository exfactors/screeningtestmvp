package suitmedia.com.dev.projectmvp.base.ui.adapter.viewholder

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding


/**
 * Created by Andri Dwi Utomo on 9/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
abstract class BaseItemViewHolder<in Any>(itemView: ViewBinding) : RecyclerView.ViewHolder(itemView.root){
    var baseContext: Context? = null
    abstract fun bind(data: Any?)
}