package suitmedia.com.dev.projectmvp.di.scope

import javax.inject.Scope


/**
 * Created by Andri Dwi Utomo on 9/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
@Scope
@Retention(value = AnnotationRetention.RUNTIME)
annotation class SuitCoreApplicationScope