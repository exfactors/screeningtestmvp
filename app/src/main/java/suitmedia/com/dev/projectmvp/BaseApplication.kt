package suitmedia.com.dev.projectmvp

import android.app.Activity
import android.content.Context
import android.content.res.Configuration
import androidx.appcompat.app.AppCompatDelegate
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.google.android.libraries.places.api.Places
import io.realm.Realm
import io.realm.RealmConfiguration
import suitmedia.com.dev.projectmvp.data.local.prefs.SuitPreferences
import suitmedia.com.dev.projectmvp.data.model.Event
import suitmedia.com.dev.projectmvp.data.model.Guest
import suitmedia.com.dev.projectmvp.data.model.GuestApi
import suitmedia.com.dev.projectmvp.data.model.Profiles
import suitmedia.com.dev.projectmvp.di.component.ApplicationComponent
import suitmedia.com.dev.projectmvp.di.component.DaggerApplicationComponent
import suitmedia.com.dev.projectmvp.di.module.ApplicationModule
import suitmedia.com.dev.projectmvp.firebase.analytics.FireBaseHelper
import suitmedia.com.dev.projectmvp.helper.ActivityLifecycleCallbacks
import suitmedia.com.dev.projectmvp.helper.CommonUtils
import suitmedia.com.dev.projectmvp.helper.localization.LanguageHelper
import suitmedia.com.dev.projectmvp.helper.rxbus.RxBus
import suitmedia.com.dev.projectmvp.onesignal.OneSignalHelper


/**
 * Created by Andri Dwi Utomo on 9/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
class BaseApplication : MultiDexApplication() {

    val mActivityLifecycleCallbacks = ActivityLifecycleCallbacks()

    init {
        instance = this
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
    }

    companion object {
        lateinit var applicationComponent: ApplicationComponent
        lateinit var appContext: Context
        lateinit var bus: RxBus
        private var instance: BaseApplication? = null

        fun currentActivity(): Activity? {
            return instance?.mActivityLifecycleCallbacks?.currentActivity
        }
    }

    override fun onCreate() {
        super.onCreate()

        registerActivityLifecycleCallbacks(mActivityLifecycleCallbacks)
        //bus = RxBus()

        // Initial Preferences
        SuitPreferences.init(applicationContext)
        FireBaseHelper.instance().initialize(this)

        CommonUtils.setDefaultBaseUrlIfNeeded()

        appContext = applicationContext
        applicationComponent = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this))
            .build()

        OneSignalHelper.initOneSignal(this)

        Places.initialize(applicationContext, getString(R.string.google_api_key))
        Places.createClient(this)

        /*Fresco.initialize(this)*/

        /*Realm.init(this)
        val realmConfiguration = RealmConfiguration.Builder()
            .schemaVersion(1)
            .allowWritesOnUiThread(true)
            .deleteRealmIfMigrationNeeded()
            //.encryptionKey(CommonUtils.getKey()) // encrypt realm
            .build()
        Realm.setDefaultConfiguration(realmConfiguration)*/

        /*val realmConfiguration = RealmConfiguration.Builder(schema = setOf(Profiles::class, Event::class, Guest::class, GuestApi::class))
            .schemaVersion(1)
            .deleteRealmIfMigrationNeeded()
            .build()
        Realm.open(realmConfiguration)*/

        /*OneSignalHelper.initOneSignal(this)

        Mapbox.getInstance(this, CommonConstant.MAP_BOX_TOKEN)

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }*/
    }

    override fun attachBaseContext(base: Context) {
        SuitPreferences.init(base)
        super.attachBaseContext(LanguageHelper.setLocale(base))
        MultiDex.install(this)
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        LanguageHelper.setLocale(this)
    }
}