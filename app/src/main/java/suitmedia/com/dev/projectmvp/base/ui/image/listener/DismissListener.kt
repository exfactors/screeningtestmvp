package suitmedia.com.dev.projectmvp.base.ui.image.listener


/**
 * Created by Andri Dwi Utomo on 14/11/2021.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
interface DismissListener {

    /**
     * This method will be invoked when the dialog is dismissed.
     */
    fun onDismiss()
}