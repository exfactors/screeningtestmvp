package suitmedia.com.dev.projectmvp.feature.screenfour

import kotlinx.coroutines.*
import retrofit2.Call
import retrofit2.Response
import suitmedia.com.dev.projectmvp.BaseApplication
import suitmedia.com.dev.projectmvp.base.presenter.BasePresenter
import suitmedia.com.dev.projectmvp.data.local.LocalDataSource
import suitmedia.com.dev.projectmvp.data.model.GuestApi
import suitmedia.com.dev.projectmvp.data.model.Responses
import suitmedia.com.dev.projectmvp.data.model.Users
import suitmedia.com.dev.projectmvp.data.remote.services.APIService
import suitmedia.com.dev.projectmvp.data.remote.wrapper.Results
import suitmedia.com.dev.projectmvp.feature.screenone.ScreenOneView
import suitmedia.com.dev.projectmvp.helper.CommonUtils
import java.io.IOException
import javax.inject.Inject
import javax.security.auth.callback.Callback
import kotlin.coroutines.CoroutineContext


/**
 * Created by Andri Dwi Utomo on 10/6/2022.
 * Mallsampah Indonesia
 * andri@mallsampah.com
 */
class ScreenFourPresenter(val view: ScreenFourView) : BasePresenter(), CoroutineScope {
    private val job = Job()
    override val coroutineContext: CoroutineContext = job + Dispatchers.IO

    private var localDataSource: LocalDataSource = LocalDataSource()

    private var mvpView: ScreenFourView? = null
    @Inject
    lateinit var apiService: APIService

    init {
        BaseApplication.applicationComponent.inject(this)
        declareView()
    }

    fun getGuestApi(page: Int, perPage: Int) = launch(Dispatchers.Main) {

        apiService.getMembersCoroutinesAsync(page, perPage).enqueue(object : retrofit2.Callback<Results<Users>> {
            override fun onResponse(call: Call<Results<Users>>, response: Response<Results<Users>>) {
                if (response.isSuccessful) {
                    try {
                        var data = response.body()?.arrayData
                        var listGuest: List<Users> = listOf()
                        data?.map {
                            localDataSource.addGuestApi(GuestApi().apply {
                                id = it.id
                                avatar = it.avatar
                                email = it.email
                                firstName = it.firstName
                                lastName = it.lastName

                            })
                        }
                        listGuest = data!!

                        var totalData = response.body()?.total
                        view.onGetGuest(listGuest, totalData!!)
                    } catch (e: IOException) {
                        view.onError(e.message.toString())
                    }
                }
            }

            override fun onFailure(call: Call<Results<Users>>, t: Throwable) {
                var data = localDataSource.getAllGuestApi()
                println(data)
                if (data != null) {
                    var listGuest : List<Users> = listOf()

                    var totalData = data?.size

                    view.onGetGuest(listGuest, totalData!!)

                } else {
                    view.onError(t.message.toString())
                }
            }

        })

    }

    fun saveGuest(name: String) = launch(Dispatchers.Main) {
        localDataSource.addGuest(name)
        view.onSuccessSave("Success")
    }

    private fun declareView() {
        mvpView = view
        addToLifeCycle(mvpView!!, job)
    }
}